FROM alpine:3.18.4

############################################################################
# Application
############################################################################
RUN apk add --no-cache unbound ca-certificates ; mkdir -p /etc/unbound/unbound.conf.d

# Copy root configuration to image
COPY ./src/unbound.conf /etc/unbound/
# Copy other configuration files to image
COPY ./src/unbound.conf.d/* /etc/unbound/unbound.conf.d/
# Copy entrypoint to image
COPY ./src/docker-entrypoint.sh /usr/local/bin/

# Get latest roots.hints from internet
# Create a low-privileged user for runnning
# Set SUID to unbound binaries
RUN wget -O /etc/unbound/root.hints https://www.internic.net/domain/named.root \
      && chmod a+x /usr/local/bin/docker-entrypoint.sh \
      && unbound-anchor -v \
      && chown -R unbound:unbound /usr/share/dnssec-root/ \
      && addgroup -S container && adduser -S container -G container \
      && chmod 4755 /usr/sbin/unbound-checkconf /usr/sbin/unbound

############################################################################
# Runtime
############################################################################
# Switch to the new user
USER container

EXPOSE 53/udp

CMD ["/usr/local/bin/docker-entrypoint.sh"]

HEALTHCHECK --interval=1m --retries=1 --start-period=5s CMD nslookup www.google.fr
