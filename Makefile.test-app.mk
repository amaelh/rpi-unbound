# Below are all project specific targets
test: test-os test-app

test-app: test-os
	@echo "##########################################################################"
	@echo "Testing single architecture image : Application"
	docker run --rm --entrypoint "" $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) sh -c 'unbound-checkconf && echo && /bin/echo "=> Successfully validated configuration" && echo ""'
	docker run --rm --entrypoint "" $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH) sh -c 'nslookup www.google.com && echo && /bin/echo "=> Successfully tested application" && echo ""'
