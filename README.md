# rpi-unbound
Unbound - caching, DNSSEC validating DNS Server for Raspberry Pi.

Original version : gorofad/rpi-unbound:latest

## General Usage
Download or checkout this project, then build the docker image with :
```
docker build -t amaelh/rpi-unbound:latest .
```
Run the container with:

```
docker run -d -p 5300:53 -p 5300:53/udp --name unbound -v /path/to/unbound/conf.d.private:/etc/unbound/unbound.conf.d.private/:ro --restart always amaelh/rpi-unbound:latest
```

## Adding local adresses
If you want to add the host name of your router, nas... copy the file `conf.d.private/04_HomeZone.conf.sample` to `conf.d.private/04_HomeZone.conf` and edit the content. Afterwards just start the container as usual.

## Debug your container
ALl logs are available in `docker logs unbound`. To change verbosity rename `conf/00_Debug.conf.sample` to `conf/00_Debug.conf` and restart the container.

You can test unbound with the `dig` command :
```
dig +dnssec google.com @127.0.0.1 -p 5300
dig +dnssec mycomputer.home.lan @127.0.0.1 -p 5300
```
