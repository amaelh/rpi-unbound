#!/bin/sh

set -ex

echo "Check Unbound config files"
unbound-checkconf

echo "Start Unbound Server"
unbound -d -d

echo "End entrypoint"
